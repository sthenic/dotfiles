code --install-extension 13xforever.language-x86-64-assembly
code --install-extension arcticicestudio.nord-visual-studio-code
code --install-extension cpmcgrath.codealignment-vscode
code --install-extension DotJoshJohnson.xml
code --install-extension eamodio.gitlens
code --install-extension Equinusocio.vsc-material-theme
code --install-extension himanoa.Python-autopep8
code --install-extension James-Yu.latex-workshop
code --install-extension jkjustjoshing.vscode-text-pastry
code --install-extension johnstoncode.svn-scm
code --install-extension jolaleye.horizon-theme-vscode
code --install-extension kosz78.nim
code --install-extension lextudio.restructuredtext
code --install-extension ms-python.python
code --install-extension ms-vscode.cpptools
code --install-extension mshr-h.veriloghdl
code --install-extension samrapdev.outrun
code --install-extension shuworks.vscode-table-formatter
code --install-extension sleutho.tcl
code --install-extension Stephanvs.dot
code --install-extension stkb.rewrap
code --install-extension streetsidesoftware.code-spell-checker
code --install-extension tomphilbin.gruvbox-themes
code --install-extension trixnz.vscode-lua
code --install-extension wayou.vscode-todo-highlight
code --install-extension xaver.clang-format
